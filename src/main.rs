fn main() {

    {
        let s = "hello";
        // s is visible inside this block 
        println!("{}! from inside the block!", s);
    };
    // not visible outside it
    //println!("{}! from outside the block!", s)

    {
        let mut s = String::from("hello");
        s.push_str(", world!");
        println!("{}", s);
    };

    {
        let x = 5;
        let y = x;
        println!("x = {}, y = {}", x, y);
    };

    {
        let s1 = String::from("hello");
        let s2 = s1;

        // s1 not accessible after been moved to s2
        //println!("{}, world", s1);
        println!("{}, world", s2);
    };

    {
        let s1 = String::from("hello");
        let s2 = s1.clone();

        // explicitly cloning s1 to s2 will make it still accessible
        println!("s1 = {}, s2 = {}", s1, s2);
    };

    {
        let s = String::from("hello");

        takes_ownership(s);

        let x = 5;

        makes_copy(x);        

        fn takes_ownership(some_string: String) {
            println!("{}", some_string);
        }

        fn makes_copy(some_integer: i32) {
            println!("{}", some_integer);
        }
    };

    {
        let s1 = gives_ownership();

        let s2 = String::from("hello");

        let s3 = takes_and_gives_back(s2);

        println!("s1 = {}", s1);
        //println!("s2 = {}", s2); // This fails, as s2 was borrowed by the function and then given to s3.
        println!("s3 = {}", s3);

        fn gives_ownership() -> String {

            let some_string = String::from("hello");

            some_string
        }

        fn takes_and_gives_back(a_string: String) -> String {

            a_string
        }
    };


    {
        let s1 = String::from("hello");

        let (s2, len) = calculate_length(s1);

        println!("The length of '{}' is {}.", s2, len);

        fn calculate_length(s: String) -> (String, usize) {
            let length = s.len();
            (s, length)
        }

    };
}

